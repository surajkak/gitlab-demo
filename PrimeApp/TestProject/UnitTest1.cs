﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PrimeApp;
namespace UnitTestProject1_prime
{
    [TestClass]
    public class UnitTest1
    {
        public object Numbers { get; private set; }

        [TestMethod]
        public void TestMethod1()
        {
            Program p = new Program();
            int result = p.Sum(2, 3);
            Assert.AreEqual(5, result);

        }

        [TestMethod]
        public void TestMethod2()
        {
            Sub s = new Sub();
            int result = s.Subs(5, 4);
            Assert.AreEqual(3, result);

        }
    }
}
